package fr.primeale.order.service;

import fr.primeale.order.model.Product;
import fr.primeale.order.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class ProductService {

  private final ProductRepository productRepository;

  public ProductService(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }


  public List<Product> getProducts() {
    log.info("Getting all products");
    Iterable<Product> products = productRepository.findAll();
    return StreamSupport.stream(products.spliterator(), false).toList();
  }
}
