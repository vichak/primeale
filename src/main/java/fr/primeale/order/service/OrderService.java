package fr.primeale.order.service;

import fr.primeale.order.exception.ResourceNotFoundException;
import fr.primeale.order.model.Order;
import fr.primeale.order.model.OrderLine;
import fr.primeale.order.repository.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class OrderService {

  private final OrderRepository orderRepository;

  public OrderService(OrderRepository orderRepository) {
    this.orderRepository = orderRepository;
  }


  public List<Order> getOrders() {
    log.info("Getting all orders");
    Iterable<Order> orders = orderRepository.findAll();
    return StreamSupport.stream(orders.spliterator(), false).toList();
  }

  public Order getOrder(Long id) {
    log.info("Getting order id {}", id);
    return orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order not found id" + id));
  }

  @Transactional()
  public Order saveOrder(Order order) {
    Order orderToSave;
    try {
      orderToSave = this.getOrder(order.getId());
      // update
      orderToSave.setClient(order.getClient());
      orderToSave.setDeliveryDate(order.getDeliveryDate());
    } catch (ResourceNotFoundException e) {
      // creation
      orderToSave = order;
    }
    calculateTotal(orderToSave);
    log.info("Saving order {}", orderToSave);
    return orderRepository.save(orderToSave);
  }

  @Transactional()
  public void deleteOrder(Long id) {
    log.info("Deleting order id {}", id);
    orderRepository.deleteById(id);
  }

  @Transactional()
  public Order addOrderLine(Long orderId, OrderLine orderLine) {
    log.info("Adding order line {} to order id {}", orderLine, orderId);
    Order order = getOrder(orderId);
    order.addOrderLine(orderLine);
    calculateTotal(order);
    return orderRepository.save(order);
  }

  @Transactional()
  public Order deleteOrderLine(Long orderId, Long orderLineId) {
    log.info("Deleting order line id {} from order id {}", orderLineId, orderId);
    Order order = getOrder(orderId);
    order.getOrderLines().removeIf(orderLine -> orderLine.getId().equals(orderLineId));
    calculateTotal(order);
    return orderRepository.save(order);
  }

  @Transactional()
  public Order updateOrderLine(Long orderId, OrderLine orderLine) {
    log.info("Updating order line {} from order id {}", orderLine, orderId);
    Order order = getOrder(orderId);
    OrderLine orderLineFromOrder = order.getOrderLines().stream().filter(ol -> ol.getId().equals(orderLine.getId())).findFirst()
      .orElseThrow(() -> new ResourceNotFoundException("OrderLine not found id" + orderLine));
    orderLineFromOrder.setUnitaryPrice(orderLine.getUnitaryPrice());
    orderLineFromOrder.setQuantity(orderLine.getQuantity());
    orderLineFromOrder.setProduct(orderLine.getProduct());
    calculateTotal(order);
    return orderRepository.save(order);
  }

  private void calculateTotal(Order order) {
    BigDecimal totalHt = new BigDecimal(0);
    BigDecimal totalTva55 = new BigDecimal(0);
    BigDecimal totalTva20 = new BigDecimal(0);
    BigDecimal totalCnipt = new BigDecimal(0);
    for (OrderLine orderLine : order.getOrderLines()) {
      BigDecimal orderLinetHt = orderLine.getUnitaryPrice().multiply(BigDecimal.valueOf(orderLine.getQuantity()));
      totalHt = totalHt.add(orderLinetHt);
      BigDecimal orderLineTva55 = orderLinetHt.multiply(BigDecimal.valueOf(0.055));
      totalTva55 = totalTva55.add(orderLineTva55);
      if (Boolean.TRUE.equals(orderLine.getProduct().getCnipt())) {
        totalCnipt = totalCnipt.add((orderLinetHt.add(orderLineTva55).multiply(BigDecimal.valueOf(0.20))));
      }
    }
    order.setTotalHt(totalHt);
    order.setTotalTva55(totalTva55);
    order.setTotalTva20(totalTva20);
    order.setTotalCnipt(totalCnipt);
    order.setTotalTtc(totalHt.add(totalTva55).add(totalTva20).add(totalCnipt));
  }
}
