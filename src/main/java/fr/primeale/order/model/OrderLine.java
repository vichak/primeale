package fr.primeale.order.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


import java.math.BigDecimal;
import java.util.Optional;

@Setter
@Getter
@Entity
@Table(name = "order_lines")
public class OrderLine {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "order_id")
  @JsonBackReference
  private Order order;

  @ManyToOne
  @JoinColumn(name = "product_id")
  private Product product;

  private Integer quantity;

  private BigDecimal unitaryPrice;

  @Override
  public String toString() {
    String orderId = (order != null) ? order.getId().toString() : null;
    return "OrderLine{" +
      "id=" + id +
      ", orderId=" + orderId +
      ", product=" + product +
      ", quantity=" + quantity +
      ", unitaryPrice=" + unitaryPrice +
      '}';
  }
}
