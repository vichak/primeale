package fr.primeale.order.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String client;

  private Date deliveryDate;

  @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<OrderLine> orderLines;

  private BigDecimal totalTtc;

  private BigDecimal totalHt;

  private BigDecimal totalTva55;

  private BigDecimal totalTva20;

  private BigDecimal totalCnipt;

  public Order() {
    this.orderLines = new ArrayList<>();
  }

  public void addOrderLine(OrderLine orderLine) {
    orderLine.setOrder(this);
    this.orderLines.add(orderLine);
  }

  @Override
  public String toString() {
    return "Order{" +
      "id=" + id +
      ", client='" + client + '\'' +
      ", deliveryDate=" + deliveryDate +
      ", orderLines=" + orderLines +
      ", totalTtc=" + totalTtc +
      ", totalHt=" + totalHt +
      ", totalTva55=" + totalTva55 +
      ", totalTva20=" + totalTva20 +
      ", totalCnipt=" + totalCnipt +
      '}';
  }
}
