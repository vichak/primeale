package fr.primeale.order.controller;

import fr.primeale.order.model.Order;
import fr.primeale.order.model.OrderLine;
import fr.primeale.order.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@RestController
public class OrderController extends ApiV1Controller {

  private final OrderService orderService;

  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @GetMapping("/orders")
  public List<Order> getOrders() {
    return orderService.getOrders();
  }

  @PostMapping("/orders")
  public Order saveOrder(@RequestBody Order order) {
    return orderService.saveOrder(order);
  }

  @GetMapping("/orders/{orderId}")
  public Order getOrder(@PathVariable Long orderId) {
    return orderService.getOrder(orderId);
  }

  @DeleteMapping("/orders/{orderId}")
  public void deleteOrder(@PathVariable Long orderId) {
    orderService.deleteOrder(orderId);
  }

  @PostMapping("/orders/{orderId}/orderLines")
  public Order addOrderLine(@PathVariable Long orderId, @RequestBody OrderLine orderLine) {
    return orderService.addOrderLine(orderId, orderLine);
  }

  @DeleteMapping("/orders/{orderId}/orderLines/{orderLineId}")
  public Order deleteOrderLine(@PathVariable Long orderId, @PathVariable Long orderLineId) {
    return orderService.deleteOrderLine(orderId, orderLineId);
  }

  @PutMapping("/orders/{orderId}/orderLines/{orderLineId}")
  public Order updateOrderLine(@PathVariable Long orderId, @PathVariable Long orderLineId, @RequestBody OrderLine orderLine) {
    if (!orderLineId.equals(orderLine.getId())) {
      throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "OrderLine id from path is different from request body");
    }
    return orderService.updateOrderLine(orderId, orderLine);
  }
}
