package fr.primeale.order.controller;

import fr.primeale.order.model.Product;
import fr.primeale.order.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController extends ApiV1Controller {

  private final ProductService productService;

  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @GetMapping("/products")
  public List<Product> getProducts() {
    return productService.getProducts();
  }
}
