import {ApplicationConfig, DEFAULT_CURRENCY_CODE, importProvidersFrom, LOCALE_ID} from '@angular/core';
import {provideRouter, RouterModule, withHashLocation} from '@angular/router';

import {routes} from './app.routes';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {provideHttpClient} from "@angular/common/http";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS} from "@angular/material/snack-bar";
import localeFr from '@angular/common/locales/fr';
import extra from '@angular/common/locales/fr';

import {registerLocaleData} from '@angular/common';
import {LoadingService} from "./services/loading.service";

registerLocaleData(localeFr, 'fr-FR', extra);

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withHashLocation()),
    importProvidersFrom([BrowserAnimationsModule, RouterModule, LoadingService]), provideHttpClient(),
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR', },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500, }, },
    { provide: LOCALE_ID, useValue: 'fr-FR', },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR', },
  ],
};
