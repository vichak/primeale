import { Routes } from '@angular/router';
import {PageNotFoundComponent} from "./pages/page-not-found/page-not-found.component";
import {OrdersListComponent} from "./pages/orders-list/orders-list.component";
import {OrderComponent} from "./pages/order/order.component";
import {HomeComponent} from "./pages/home/home.component";
import {NewOrderComponent} from "./pages/new-order/new-order.component";
import {AddOrderLineComponent} from "./pages/order/add-order-line/add-order-line.component";
import {EditOrderLineComponent} from "./pages/order/edit-order-line/edit-order-line.component";
import {EditOrderComponent} from "./pages/order/edit-order/edit-order.component";

export const routes: Routes = [
   {
    path: 'accueil',
    title: 'Priméale | Accueil',
    component: HomeComponent
  },
  {
    path: 'commandes', children: [
      {
        path: 'toutes',
        title: 'Priméale | Commandes',
        component: OrdersListComponent
      }, {
        path: 'nouvelle',
        title:'Priméale | Nouvelle commande',
        component: NewOrderComponent
      }, {
        path: ':id',
        children: [
          {
            path: 'modifier',
            title: 'Priméale | Modifier la commande',
            component: EditOrderComponent
          },
          {
            path: 'ajouter-article',
            title: 'Priméale | Modifier la commande - Ajouter un article',
            component: AddOrderLineComponent
          },
          {
            path: 'modifier-article/:idOrderLine',
            title: 'Priméale | Modifier la commande - Modifier un article',
            component: EditOrderLineComponent
          },
          {
            path: '',
            title: 'Priméale | Détails de la commande',
            component: OrderComponent
          }
        ]
      }, {
        path: '',
        redirectTo: 'toutes', pathMatch: 'full'
      }
    ]
  },
  {
    path: '', redirectTo: 'accueil', pathMatch: 'full'
  },
  {
    path: 'page-non-trouvee', title: 'Priméale | Page non trouvée', component: PageNotFoundComponent
  },
  {
    path: '**', title: 'Priméale | Page non trouvée', component: PageNotFoundComponent
  }
];
