import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Product} from "../models/product";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private _httpclient: HttpClient) {
  }

  getProducts(): Observable<Product[]> {
    return this._httpclient.get<Product[]>('api/v1/products')
  }
}
