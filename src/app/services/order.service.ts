import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Order} from "../models/order";
import {OrderSaveDto} from "../models/order-save-dto";
import {OrderLine} from "../models/orderLine";

@Injectable({
  providedIn: 'root',
})
export class OrderService {

  constructor(private httpclient: HttpClient) {
  }

  getOrders(): Observable<Order[]> {
    return this.httpclient.get<Order[]>('api/v1/orders')
  }

  saveOrder(order: OrderSaveDto): Observable<Order> {
    return this.httpclient.post<Order>('api/v1/orders', order)
  }

  getOrder(id: number): Observable<Order> {
    return this.httpclient.get<Order>(`api/v1/orders/${id}`)
  }

  addOrderLine(orderId: number, orderLine: OrderLine): Observable<Order> {
    return this.httpclient.post<Order>(`api/v1/orders/${orderId}/orderLines`, orderLine)
  }

  updateOrderLine(orderId: number, orderLine: OrderLine): Observable<Order> {
    return this.httpclient.put<Order>(`api/v1/orders/${orderId}/orderLines/${orderLine.id}`, orderLine);
  }

  removeOrderLine(orderId: number, orderLineId: number): Observable<Order> {
    return this.httpclient.delete<Order>(`api/v1/orders/${orderId}/orderLines/${orderLineId}`);
  }

  deleteOrder(id: number): Observable<void> {
    return this.httpclient.delete<void>(`api/v1/orders/${id}`)
  }
}
