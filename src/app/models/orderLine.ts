import {Product} from "./product";

export class OrderLine {
  id: number| undefined
  product: Product | undefined
  quantity: number = 0;
  unitaryPrice: number = 0;
}
