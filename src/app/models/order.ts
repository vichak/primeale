import {OrderLine} from "./orderLine";

export class Order {
  id: number | undefined;
  orderLines: OrderLine[] = [];
  deliveryDate: Date | undefined;
  client: string | undefined;
  totalTtc: number = 0;
  totalHt: number = 0;
  totalTva55: number = 0;
  totalTva20: number = 0;
  totalCnipt: number = 0;
}
