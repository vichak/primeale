export class OrderSaveDto {
  id: number | undefined | null;
  deliveryDate: Date | undefined | null;
  client: string | undefined | null;
}
