import {Component, OnInit, ViewChild} from '@angular/core';
import {OrderService} from "../../services/order.service";
import {AbstractOnDestroyComponent} from "../../components/abstract-on-destroy/abstract-on-destroy.component";
import {finalize, takeUntil} from "rxjs";
import {Order} from "../../models/order";
import {MatButtonModule} from "@angular/material/button";
import {RouterLink} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MatTable, MatTableModule} from "@angular/material/table";
import {OrderLine} from "../../models/orderLine";
import {LoadingService} from "../../services/loading.service";

@Component({
  selector: 'app-orders-list',
  standalone: true,
    imports: [
        CommonModule,
        MatButtonModule,
        RouterLink,
        MatTableModule,
    ],
  templateUrl: './orders-list.component.html',
  styleUrl: './orders-list.component.css'
})
export class OrdersListComponent extends AbstractOnDestroyComponent implements OnInit {
  @ViewChild(MatTable) table: MatTable<OrderLine> | undefined;
  displayedColumns = ['id', 'client', 'deliveryDate', 'totalHt', 'actions'];
  orders: Order[] = [];

  constructor(private orderService: OrderService, private loadingService: LoadingService) {
    super();
  }

  ngOnInit(): void {
    this.loadingService.loadingOn();
    this.orderService.getOrders()
      .pipe(takeUntil(this.unsubscribe), finalize(() => this.loadingService.loadingOff()))
      .subscribe(orders => {
        this.orders = orders;
        this.table?.renderRows();
      });
  }
}
