import { Component } from '@angular/core';
import {FormOrderComponent} from "../../components/form-order/form-order.component";

@Component({
  selector: 'app-new-order',
  standalone: true,
  templateUrl: './new-order.component.html',
  imports: [
    FormOrderComponent
  ],
  styleUrl: './new-order.component.css'
})
export class NewOrderComponent {

}
