import {Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {Order} from "../../models/order";
import {CommonModule, DatePipe, DecimalPipe} from "@angular/common";
import {OrderService} from "../../services/order.service";
import {finalize, takeUntil} from "rxjs";
import {MatButton, MatButtonModule} from "@angular/material/button";
import {MatTable, MatTableModule} from "@angular/material/table";
import {OrderLine} from "../../models/orderLine";
import {AbstractOrderComponent} from "./abstract-order.component";
import {LoadingService} from "../../services/loading.service";

@Component({
  selector: 'app-order',
  standalone: true,
  templateUrl: './order.component.html',
  imports: [
    CommonModule,
    DecimalPipe,
    DatePipe,
    MatButton,
    RouterLink,
    MatButtonModule,
    MatTableModule
  ],
  styleUrl: './order.component.css'
})
export class OrderComponent extends AbstractOrderComponent {
  @ViewChild(MatTable) table: MatTable<OrderLine> | undefined;
  displayedColumns: string[] = ['name', 'quantity', 'unitaryPrice', 'totalHt', 'action'];
  dataSource: OrderLine[] =  [];

  constructor(
    override _router: Router,
    override _route: ActivatedRoute,
    override _orderService: OrderService,
    override _loadingService: LoadingService,
  ) {
    super(_router, _route, _orderService, _loadingService);
  }

  override onOrderInit() {
    this.updateDataSource();
  }

  private updateDataSource() {
    if (this.table) {
      this.dataSource = this.order.orderLines;
      this.table.renderRows();
    }
  }

  removeOrderLine(orderLine: OrderLine) {
    if (this.order.id && orderLine.id && confirm('Voulez-vous vraiment supprimer cette ligne ?')) {
      this._loadingService.loadingOn()
      this._orderService.removeOrderLine(this.order.id, orderLine.id)
        .pipe(takeUntil(this.unsubscribe), finalize(() => this._loadingService.loadingOff()))
        .subscribe((order: Order) => {
          this.order = order;
          this.updateDataSource();
        });
    }
  }

  deleteOrder() {
    if (this.order.id && confirm('Voulez-vous vraiment supprimer cette commande ?')) {
      this._loadingService.loadingOn();
      this._orderService.deleteOrder(this.order.id)
        .pipe(takeUntil(this.unsubscribe), finalize(() => this._loadingService.loadingOff()))
        .subscribe(() => this._router.navigate(['/commandes']));
    }
  }
}
