import {Injectable, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Order} from "../../models/order";
import {AbstractOnDestroyComponent} from "../../components/abstract-on-destroy/abstract-on-destroy.component";
import {OrderService} from "../../services/order.service";
import {finalize, takeUntil} from "rxjs";
import {LoadingService} from "../../services/loading.service";

@Injectable()
export abstract class AbstractOrderComponent extends AbstractOnDestroyComponent implements OnInit {

  @Input() order: Order = new Order();

  protected constructor(protected _router: Router, protected _route: ActivatedRoute, protected _orderService: OrderService, protected _loadingService: LoadingService) {
    super();
  }

  ngOnInit() {
    this._loadingService.loadingOn();
    this._route.params.pipe(takeUntil(this.unsubscribe)).subscribe(params => {
      if (params['id'] && this.order?.id !== params['id']) {
        this._orderService.getOrder(params['id'])
          .pipe(takeUntil(this.unsubscribe), finalize(()=> this._loadingService.loadingOff()))
          .subscribe(order => {
            this.order = order;
            this.onOrderInit()
          }, (error) => {
            if (error.status === 404) {
              this._router.navigate(['/page-non-trouvee'], {skipLocationChange: true});
            } else {
              console.log(error);
              this._router.navigate(['/commandes']);
            }
          });
      }
    });
  }

  // call by child
  onOrderInit() {
  }
}
