import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of, throwError } from 'rxjs';
import {FormOrderLineComponent} from "../../../components/form-order-line/form-order-line.component";
import {ProductService} from "../../../services/product.service";
import {OrderService} from "../../../services/order.service";
import {LoadingService} from "../../../services/loading.service";
import {Order} from "../../../models/order";
import {HttpClientModule} from "@angular/common/http";


describe('FormOrderLineComponent', () => {
  let component: FormOrderLineComponent;
  let fixture: ComponentFixture<FormOrderLineComponent>;
  let mockProductService;
  let mockOrderService: any;
  let mockSnackBar: any;
  let mockRouter: any;
  let mockLoadingService: any;

  beforeEach(async () => {
    mockProductService = jasmine.createSpyObj(['getProducts']);
    mockOrderService = jasmine.createSpyObj(['addOrderLine', 'updateOrderLine']);
    mockSnackBar = jasmine.createSpyObj(['open']);
    mockRouter = jasmine.createSpyObj(['navigate']);
    mockLoadingService = jasmine.createSpyObj(['loadingOn', 'loadingOff']);

    await TestBed.configureTestingModule({
      imports: [HttpClientModule, ReactiveFormsModule],
      providers: [
        { provide: ProductService, useValue: mockProductService },
        { provide: OrderService, useValue: mockOrderService },
        { provide: MatSnackBar, useValue: mockSnackBar },
        { provide: Router, useValue: mockRouter },
        { provide: LoadingService, useValue: mockLoadingService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FormOrderLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not submit if form is invalid', () => {
    component.orderLineForm.setErrors({ invalid: true });
    component.onSubmit();
    expect(mockOrderService.addOrderLine).not.toHaveBeenCalled();
    expect(mockOrderService.updateOrderLine).not.toHaveBeenCalled();
  });

  it('should submit if form is valid and orderLine id is not defined', () => {
    const order = new Order();
    order.id = 1;
    const orderLine = { id: null, product: {}, quantity: 1, unitaryPrice: 1 };
    component.order = order;
    component.orderLineForm.setValue(orderLine);
    mockOrderService.addOrderLine.and.returnValue(of(order));

    component.onSubmit();

    expect(mockOrderService.addOrderLine).toHaveBeenCalledWith(order.id, orderLine);
    expect(mockSnackBar.open).toHaveBeenCalledWith('Ligne de commande sauvegardée');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/commandes', order.id]);
  });

  it('should submit if form is valid and orderLine id is defined', () => {
    const order = new Order();
    order.id = 1;
    const orderLine = { id: 1, product: {}, quantity: 1, unitaryPrice: 1 };
    component.order = order;
    component.orderLineForm.setValue(orderLine);
    mockOrderService.updateOrderLine.and.returnValue(of(order));

    component.onSubmit();

    expect(mockOrderService.updateOrderLine).toHaveBeenCalledWith(order.id, orderLine);
    expect(mockSnackBar.open).toHaveBeenCalledWith('Ligne de commande sauvegardée');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/commandes', order.id]);
  });

  it('should handle error when saving orderLine fails', () => {
    const order = new Order();
    order.id = 1;
    const orderLine = { id: null, product: {}, quantity: 1, unitaryPrice: 1 };
    component.order = order;
    component.orderLineForm.setValue(orderLine);
    mockOrderService.addOrderLine.and.returnValue(throwError('error'));

    component.onSubmit();

    expect(mockSnackBar.open).toHaveBeenCalledWith('Erreur lors de la sauvegarde de la ligne de commande');
  });
});
