import {Component} from '@angular/core';
import {FormOrderComponent} from "../../../components/form-order/form-order.component";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {AbstractOrderComponent} from "../abstract-order.component";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../../../services/order.service";
import {LoadingService} from "../../../services/loading.service";

@Component({
  selector: 'app-edit-order',
  standalone: true,
  imports: [FormOrderComponent, MatProgressSpinner],
  templateUrl: './edit-order.component.html',
  styleUrl: './edit-order.component.css'
})
export class EditOrderComponent extends AbstractOrderComponent {
  constructor(override _router: Router, override _route: ActivatedRoute, override _orderService: OrderService,override _loadingService: LoadingService,) {
    super(_router, _route, _orderService, _loadingService);
  }
}
