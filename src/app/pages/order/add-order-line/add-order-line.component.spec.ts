import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOrderLineComponent } from './add-order-line.component';

describe('AddOrderLineComponent', () => {
  let component: AddOrderLineComponent;
  let fixture: ComponentFixture<AddOrderLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddOrderLineComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AddOrderLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
