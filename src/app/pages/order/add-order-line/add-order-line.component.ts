import {Component} from '@angular/core';
import {FormOrderLineComponent} from "../../../components/form-order-line/form-order-line.component";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../../../services/order.service";
import {AbstractOrderComponent} from "../abstract-order.component";
import {LoadingService} from "../../../services/loading.service";

@Component({
  selector: 'app-add-order-line',
  standalone: true,
  templateUrl: './add-order-line.component.html',
  imports: [FormOrderLineComponent],
  styleUrl: './add-order-line.component.css'
})
export class AddOrderLineComponent extends AbstractOrderComponent {
  constructor(override _router: Router, override _route: ActivatedRoute, override _orderService: OrderService, override _loadingService: LoadingService,) {
    super(_router, _route, _orderService, _loadingService);
  }
}
