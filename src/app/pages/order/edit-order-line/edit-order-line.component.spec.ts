import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOrderLineComponent } from './edit-order-line.component';

describe('EditOrderLineComponent', () => {
  let component: EditOrderLineComponent;
  let fixture: ComponentFixture<EditOrderLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EditOrderLineComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EditOrderLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
