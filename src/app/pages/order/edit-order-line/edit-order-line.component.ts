import {Component} from '@angular/core';
import {takeUntil} from "rxjs";
import {ActivatedRoute, Router, RouterModule} from "@angular/router";
import {OrderService} from "../../../services/order.service";
import {OrderLine} from "../../../models/orderLine";
import {FormOrderLineComponent} from "../../../components/form-order-line/form-order-line.component";
import {MatButtonModule} from "@angular/material/button";
import {AbstractOrderComponent} from "../abstract-order.component";
import {LoadingService} from "../../../services/loading.service";

@Component({
  selector: 'app-edit-order-line',
  standalone: true,
  imports: [FormOrderLineComponent, RouterModule, MatButtonModule,],
  templateUrl: './edit-order-line.component.html',
  styleUrl: './edit-order-line.component.css'
})
export class EditOrderLineComponent extends AbstractOrderComponent {
  orderLine: OrderLine = new OrderLine();

  constructor(
    override _router: Router,
    override _route: ActivatedRoute,
    override _orderService: OrderService,
    override _loadingService: LoadingService,
    ) {
    super(_router, _route, _orderService, _loadingService);
  }

  override onOrderInit() {
    this._route.params.pipe(takeUntil(this.unsubscribe)).subscribe(params => {
      const foundOrderline = this.order.orderLines.find(value => value.id?.toString() === params['idOrderLine']);
      if (foundOrderline) {
        this.orderLine = foundOrderline;
      } else {
        this._router.navigate(['/page-non-trouvee'], {skipLocationChange: true});
      }
    });
  }
}
