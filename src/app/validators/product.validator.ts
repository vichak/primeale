import {AbstractControl, ValidationErrors} from '@angular/forms';
import {Product} from "../models/product";

export class ProductValidators {
  static required(control: AbstractControl): ValidationErrors | null {
    if (control.value === null || control.value === undefined || control.value instanceof Product && control.value.id === 0) return {required: true};
    return null;
  }
}
