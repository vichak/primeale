import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Product} from "../../models/product";
import {AbstractControl, FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {ProductService} from "../../services/product.service";
import {finalize, Observable, takeUntil} from "rxjs";
import {AbstractOnDestroyComponent} from "../abstract-on-destroy/abstract-on-destroy.component";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatButtonModule} from "@angular/material/button";
import {OrderService} from "../../services/order.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Order} from "../../models/order";
import {Router} from "@angular/router";
import {CommonModule} from "@angular/common";
import {ProductValidators} from "../../validators/product.validator";
import {OrderLine} from "../../models/orderLine";
import {LoadingService} from "../../services/loading.service";

@Component({
  selector: 'app-form-order-line',
  standalone: true,
  imports: [CommonModule, MatProgressSpinner, ReactiveFormsModule, MatInputModule, MatSelectModule, MatButtonModule,],
  templateUrl: './form-order-line.component.html',
  styleUrl: './form-order-line.component.css'
})
export class FormOrderLineComponent extends AbstractOnDestroyComponent implements OnInit, OnChanges {
  products: Product[] = [];
  @Input() order: Order | undefined;
  @Input() orderLine: OrderLine | undefined;

  submitted = false;
  orderLineForm: FormGroup = new FormGroup({
    id: new FormControl<number | null>(null),
    product: new FormControl<Product>(new Product(), ProductValidators.required),
    quantity: new FormControl<number>(1, Validators.required),
    unitaryPrice: new FormControl<number | null>(null, Validators.required),
  });

  constructor(private productService: ProductService, private orderService: OrderService, private snackBar: MatSnackBar, private router: Router, private loadingService: LoadingService) {
    super();
  }

  ngOnInit() {
    this.loadingService.loadingOn();
    this.productService.getProducts()
      .pipe(takeUntil(this.unsubscribe), finalize(() => this.loadingService.loadingOff()))
      .subscribe(products => this.products = products);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['orderLine']) {
      this.orderLineForm.patchValue(changes['orderLine'].currentValue);
    }
  }

  compareProductFn(p1: Product, p2: Product): boolean {
    return p1 && p2 ? p1.id === p2.id : p1 === p2;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.orderLineForm.invalid) return;
    let orderUpdate$: Observable<Order>;
    if (!this.order?.id) {
      this.snackBar.open("Erreur lors de la sauvegarde de la ligne de commande");
      return;
    } else if (this.orderLine?.id) {
      // modification
      orderUpdate$ = this.orderService.updateOrderLine(this.order.id, this.orderLineForm.value);
    } else {
      // creation
      orderUpdate$ = this.orderService.addOrderLine(this.order.id, this.orderLineForm.value);

    }
    orderUpdate$.pipe(takeUntil(this.unsubscribe)).subscribe((orderUpdated: Order) => {
      this.snackBar.open("Ligne de commande sauvegardée");
      this.router.navigate(['/commandes', this.order?.id]);
    }, () => {
      this.snackBar.open("Erreur lors de la sauvegarde de la ligne de commande");
    });
  }

  hasError(abstractControl: AbstractControl): boolean {
    return (
      (this.submitted && abstractControl.invalid) ||
      (abstractControl.invalid &&
        (abstractControl.dirty || abstractControl.touched))
    );
  }
}
