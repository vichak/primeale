import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormOrderLineComponent } from './form-order-line.component';

describe('FormOrderLineComponent', () => {
  let component: FormOrderLineComponent;
  let fixture: ComponentFixture<FormOrderLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormOrderLineComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormOrderLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
