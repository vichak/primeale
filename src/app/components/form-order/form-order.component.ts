import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {Router, RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatNativeDateModule} from "@angular/material/core";
import {Order} from "../../models/order";
import {OrderService} from "../../services/order.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {takeUntil} from "rxjs";
import {AbstractOnDestroyComponent} from "../abstract-on-destroy/abstract-on-destroy.component";

@Component({
  selector: 'app-form-order',
  standalone: true,
  imports: [
    RouterModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
  ],
  templateUrl: './form-order.component.html',
  styleUrl: './form-order.component.css'
})
export class FormOrderComponent extends AbstractOnDestroyComponent implements OnChanges {
  @Input() order: Order | undefined;

  submitted = false;
  orderForm = new FormGroup({
    id: new FormControl<number>(0),
    client: new FormControl<string>('', Validators.required),
    deliveryDate: new FormControl<Date>(new Date(), {validators: [Validators.required]}),
  });

  constructor(
    private _orderService: OrderService,
    private _snackBar: MatSnackBar,
    private _router: Router,
  ) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['order'] && changes['order'].currentValue) {
      this.orderForm.patchValue(changes['order'].currentValue);
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.orderForm.invalid) return;
    const {id, deliveryDate, client} = this.orderForm.value;
    this._orderService.saveOrder({id, deliveryDate, client})
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((order: Order) => {
        this._snackBar.open("Commande sauvegardée");
        this._router.navigate(['/commandes', order.id]);
      }, () => {
        this._snackBar.open("Erreur lors de la sauvegarde");
      });
  }

  hasError(abstractControl: AbstractControl): boolean {
    return (
      (this.submitted && abstractControl.invalid) ||
      (abstractControl.invalid &&
        (abstractControl.dirty || abstractControl.touched))
    );
  }
}
