import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of, throwError } from 'rxjs';

import { FormOrderComponent } from './form-order.component';
import { OrderService } from '../../services/order.service';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('FormOrderComponent', () => {
  let component: FormOrderComponent;
  let fixture: ComponentFixture<FormOrderComponent>;
  let mockOrderService: any;
  let mockSnackBar: any;
  let mockRouter: any;

  beforeEach(async () => {
    mockOrderService = jasmine.createSpyObj(['saveOrder']);
    mockSnackBar = jasmine.createSpyObj(['open']);
    mockRouter = jasmine.createSpyObj(['navigate']);

    await TestBed.configureTestingModule({
      imports: [NoopAnimationsModule, ReactiveFormsModule],
      providers: [
        { provide: OrderService, useValue: mockOrderService },
        { provide: MatSnackBar, useValue: mockSnackBar },
        { provide: Router, useValue: mockRouter },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FormOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not submit if form is invalid', () => {
    component.orderForm.setErrors({ invalid: true });
    component.onSubmit();
    expect(mockOrderService.saveOrder).not.toHaveBeenCalled();
  });

  it('should submit if form is valid', () => {
    const order = { id: 1, deliveryDate: new Date(), client: 'Client' };
    component.orderForm.setValue(order);
    mockOrderService.saveOrder.and.returnValue(of(order));

    component.onSubmit();

    expect(mockOrderService.saveOrder).toHaveBeenCalledWith(order);
    expect(mockSnackBar.open).toHaveBeenCalledWith('Commande sauvegardée');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/commandes', order.id]);
  });

  it('should handle error when saving order fails', () => {
    const order = { id: 1, deliveryDate: new Date(), client: 'Client' };
    component.orderForm.setValue(order);
    mockOrderService.saveOrder.and.returnValue(throwError('error'));

    component.onSubmit();

    expect(mockSnackBar.open).toHaveBeenCalledWith('Erreur lors de la sauvegarde');
  });
});
