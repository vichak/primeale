package fr.primeale.order.mock;

import fr.primeale.order.model.Product;

public final class ProductMock {
  public ProductMock() {
    throw new IllegalStateException("Utility class");
  }
  public static Product createProduct1() {
    Product product = new Product();
    product.setId(1L);
    product.setName("Product 1");
    product.setCnipt(false);
    return product;
  }

  public static Product createProduct2() {
    Product product = new Product();
    product.setId(2L);
    product.setName("Product 2");
    product.setCnipt(true);
    return product;
  }
}
