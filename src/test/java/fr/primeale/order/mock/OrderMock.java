package fr.primeale.order.mock;

import fr.primeale.order.model.Order;
import fr.primeale.order.model.OrderLine;

import java.math.BigDecimal;

public final class OrderMock {
  public OrderMock() {
    throw new IllegalStateException("Utility class");
  }

  public static Order createOrder1() {
    Order order = new Order();
    order.setId(1L);
    order.setClient(ClientMock.createClient1());
    return order;
  }

  public static OrderLine createOrderLine1(Order order) {
    OrderLine orderLine = new OrderLine();
    orderLine.setId(1L);
    orderLine.setOrder(order);
    orderLine.setProduct(ProductMock.createProduct1());
    orderLine.setQuantity(1);
    orderLine.setUnitaryPrice(BigDecimal.valueOf(10));
    return orderLine;
  }

  public static OrderLine createOrderLine2(Order order) {
    OrderLine orderLine = new OrderLine();
    orderLine.setId(2L);
    orderLine.setOrder(order);
    orderLine.setProduct(ProductMock.createProduct2());
    orderLine.setQuantity(1);
    orderLine.setUnitaryPrice(BigDecimal.valueOf(10));
    return orderLine;
  }
}
