package fr.primeale.order.mock;

public final class ClientMock {
  public ClientMock() {
    throw new IllegalStateException("Utility class");
  }

  public static String createClient1() {
    return "Client 1\n12 rue de l'église\n50430 Lessay\nFRANCE";
  }
}
