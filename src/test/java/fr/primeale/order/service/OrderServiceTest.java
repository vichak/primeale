package fr.primeale.order.service;

import fr.primeale.order.exception.ResourceNotFoundException;
import fr.primeale.order.mock.OrderMock;
import fr.primeale.order.model.Order;
import fr.primeale.order.model.OrderLine;
import fr.primeale.order.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderServiceTest {

  @Mock
  private OrderRepository orderRepository;

  @InjectMocks
  private OrderService orderService;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void getOrdersReturnsAllOrders() {
    when(orderRepository.findAll()).thenReturn(Arrays.asList(new Order(), new Order()));
    assertEquals(2, orderService.getOrders().size());
  }

  @Test
  void getOrderReturnsOrderWhenExists() {
    Order order = new Order();
    when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
    assertEquals(order, orderService.getOrder(1L));
  }

  @Test
  void getOrderThrowsExceptionWhenOrderDoesNotExist() {
    when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());
    assertThrows(ResourceNotFoundException.class, () -> orderService.getOrder(1L));
  }

  @Test
  void saveOrderSavesAndReturnsOrder() {
    Order order = new Order();
    when(orderRepository.save(any(Order.class))).thenReturn(order);
    assertEquals(order, orderService.saveOrder(new Order()));
  }

  @Test
  void deleteOrderDeletesOrder() {
    doNothing().when(orderRepository).deleteById(anyLong());
    assertDoesNotThrow(() -> orderService.deleteOrder(1L));
  }

  @Test
  void addOrderLineAddsLineAndReturnsOrder() {
    Order order = OrderMock.createOrder1();
    when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
    when(orderRepository.save(any(Order.class))).thenReturn(order);
    orderService.addOrderLine(order.getId(), OrderMock.createOrderLine1(order));
    assertEquals(1, order.getOrderLines().size());
  }

  @Test
  void deleteOrderLineDeletesLineAndReturnsOrder() {
    Order order = OrderMock.createOrder1();
    List<OrderLine> orderLines = new ArrayList<>();
    orderLines.add(OrderMock.createOrderLine1(order));
    orderLines.add(OrderMock.createOrderLine2(order));
    order.setOrderLines(orderLines);
    when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
    when(orderRepository.save(any(Order.class))).thenReturn(order);
    orderService.deleteOrderLine(order.getId(), 1L);
    assertEquals(1, order.getOrderLines().size());
  }

  @Test
  void addOrderLine1AndTestTotal1() {
    Order order = OrderMock.createOrder1();
    when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
    when(orderRepository.save(any(Order.class))).thenReturn(order);
    orderService.addOrderLine(order.getId(), OrderMock.createOrderLine1(order));
    assertEquals(BigDecimal.valueOf(10).setScale(2, RoundingMode.CEILING), order.getTotalHt().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(10.55).setScale(2, RoundingMode.CEILING), order.getTotalTtc().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(0.55).setScale(2, RoundingMode.CEILING), order.getTotalTva55().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(0).setScale(2, RoundingMode.CEILING), order.getTotalTva20().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(0).setScale(2, RoundingMode.CEILING), order.getTotalCnipt().setScale(2, RoundingMode.CEILING));
  }

  @Test
  void addOrderLine1AndTestTotalCnipt() {
    Order order = OrderMock.createOrder1();
    when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
    when(orderRepository.save(any(Order.class))).thenReturn(order);
    orderService.addOrderLine(order.getId(), OrderMock.createOrderLine2(order));
    assertEquals(BigDecimal.valueOf(10).setScale(2, RoundingMode.CEILING), order.getTotalHt().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(12.66).setScale(2, RoundingMode.CEILING), order.getTotalTtc().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(0.55).setScale(2, RoundingMode.CEILING), order.getTotalTva55().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(0).setScale(2, RoundingMode.CEILING), order.getTotalTva20().setScale(2, RoundingMode.CEILING));
    assertEquals(BigDecimal.valueOf(2.11).setScale(2, RoundingMode.CEILING), order.getTotalCnipt().setScale(2, RoundingMode.CEILING));
  }
}
